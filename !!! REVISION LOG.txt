2020-6-8	-	Added Kyle's Libraries (Bio-Analysis Chamber Project)
2020-6-11	-	Added to (DAC) - AD9833 Waveform Generator
2020-7-8	-	Added to (Ferrites) - CVH160808-2R2M
			Added to (Crystals-Oscillators) - FD2500090
			Added to (LDO) - TLV75525PDBVR
			Added to (Switch ICs) - TMUX1204DQAR-SCH
			Added to (Power Management ICs) - TPS62163DSGR
			Added to (Connectors - Headers) - 30340-6002HB
2020-7-14	-	Added to (Amplifier ICs) - LT1711CMS8#PBF
						 - LT1711CMS8#PBF_SEPERATE
			Added to (Logic ICs) - 74LVC2G14DW-7
					     - 74LVC2G14DW-7_SEPERATE
					     - SN74AUC04RGYR_SEPERATE
					     - SN74AUP1G02DCKR
					     - SN74AUP1G02DCKR_SEPERATE
					     - SN74AUP1G32DCKR
					     - SN74AUP1G32DCKR_SEPERATE
					     - SN74HC08ANSR
					     - SN74HC08ANSR_SEPERATE
					     - SN74HC08PWR
					     - SN74HC08PWR_AND_SEPERATE
					     - SN74HC86PWR
					     - SN74HC86PWR_SEPERATE
					     - SN74LVC1G11DCKRE4
					     - SN74LVC1G11DCKRE4_SEPERATE
					     - SN74LVC2G125DCUR_SEPERATE
			Added to (Connectors - Headers) - 61300311121
			Added to (Connectors - Power Supply) - PJ-002AH-SMT-TR
			Added to (Capacitors) - CC0603KRX5R8BB105
					      - CC0603KRX7R9BB104
					      - GRM188R61E106KA73D
			Added to (Power Management ICs) - LM2662MX_NOPB
			Added to (LDO) - NCP3335ADM250R2G
				       - NCP3335ADM330R2G
				       - R1154N100B-TR-FE
			Added to (Circuit Protection) - MF-MSMF050-2
2020-7-30	-	Added to (Connectors - Headers) - 30314-6003HB
			Added to (Electromechanical) - ADF08T04
						     - TXS2SA-L2-4.5V-Z
			Added to (Switch ICs) - 74LVC1G3157GV,125
					      - 74LVC1G3157GV,125_KAILEE
					      - TMUX1204DQAR
					      - TMUX1204DQAR_AARON
					      - TMUX1209PWR_AARON
			Added to (Crystals-Oscillators) - EC3645TTS-25.000M_KAILEE
							- ECS-3225S25-250-FN-TR
							- FD2500090_AARON
			Added to (Operational Amplifier) - MCP6292IDGKT
			Added to (Amplifier ICs) - OPA2320SAIDGSR
			Added to (DAC) - AD9833BRMZ-REEL7
2020-8-2	-	Added to (Connectors - Headers) - 811-22-005-30-002101
							- 30314-5002HB
			Added to (Amplifier ICs) - LMP7721MA_NOPB
						 - OPA2320SAIDGSR 			(REPLACED WITH UPDATED LIBRARY)
			Added to (Power Management ICs) - LTC1550LCS8-2#PBF
							- SZNUD3124DMT1G
							- SZNUD3160LT1G
			Added to (Switch ICs) - MAX4638EUE+T
					      - MAX4638EUE+T_DUPLICATE
					      - MAX4639EUE+T
					      - MAX4639EUE+T_SEPERATE
			Added to (Analog Switch ICs) - NLAS4157DFT2G
						     - NLAS4157DFT2G_SEPERATE
			Added to (Interface IC) - PCAL6524HEAZ
			Added to (Logic ICs) - PI4ULS5V108LEX
					     - SN74LVC1G08DCKR_SEPERATE
			Added to (LDO) - S-812C20AMC-C2AT2U
				       - TAR5S50TE85LF
2020-9-2	-	Added to (Logic ICs) - SN74LVC2G32DCTRE4
					     - SN74LVC2G32DCTRE4_SEPERATE
